window.onload = function () {
    loadTable();
};

function loadTable() {
    // Find the table
    // let TblHead = document.getElementById("TableHead");
    let url = "https://jsonplaceholder.typicode.com/users";
    fetch(url)
        .then((response) => response.json())
        .then((data) => {

            let numPeople = data.length;
            for (let i = 0; i < numPeople; i++) {
                //   buildrows(tbody, data[i]);
             

                let tbody = document.getElementById("userTable");
                let row = tbody.insertRow(-1);

                // Create new cells (<td> elements) and add text
                let cell1 = row.insertCell(0);
                cell1.innerHTML = data[i].name;
                let cell2 = row.insertCell(1);
                cell2.innerHTML = data[i].username;
                let cell3 = row.insertCell(2);
                cell3.innerHTML = data[i].email;
                let cell4 = row.insertCell(3);
                cell4.innerHTML = data[i].address.street;
                let cell5 = row.insertCell(4);
                cell5.innerHTML = data[i].address.city;
                let cell6 = row.insertCell(5);
                cell5.innerHTML = data[i].address.zipcode;

            }
        });
    // loop through the array

};

