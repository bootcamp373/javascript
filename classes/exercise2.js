class person {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // The getFullName() method returns a string that combines
    // the first and last names
    getFullName() {
        return this.firstName + " " + this.lastName;
    }
    // The getIntro() method returns a string
    getIntro() {
        let intro =
            "Hi! I'm " + this.getFullName() + " and I am a " +
            this.jobTitle;
        return intro;
    }
    // promote (jobTitle, payRate) { 
    //   this.jobTitle = jobTitle;
    //   this.payRate = payRate;
    // }
}


let person1 = new person(
    "Cody", "Allison");

console.log(`Full Name ${person1.getFullName()} `);

class Student extends person {
    constructor(firstName, lastName, grade, major) {
        super(firstName, lastName);
        this.grade = grade
        this.major = major
    }
    // You can also use methods defined in the parent class
    // from the extended class
    getIntro() {
        return "My name is " + this.getFullName() +
            " and I am a " + this.grade +
            " and I study " + this.major + "!";
    }
}
let tim = new Student(
    "Tim", "Kellogg", "Sophomore", "Engineering");
console.log(tim.getIntro());

let cody = new Student(
    "Cody", "Allison", "Senior", "Business");
console.log(cody.getIntro());

class pay extends person {
    constructor(firstName, lastName, payRate, hoursWorked) {
        super(firstName, lastName);
        this.payRate = payRate
        this.hoursWorked = hoursWorked
    }
    // You can also use methods defined in the parent class
    // from the extended class
    getPayRate() {
        return "My name is " + this.getFullName() +
            " and my gross pay is " + this.payRate * this.hoursWorked;
    }
}
let cody2 = new pay(
    "Cody", "Allison", "50", "40");
console.log(cody2.getPayRate());