class Employee {
  constructor(id, firstName, lastName, jobTitle, payRate) {
    this.employeeId = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.jobTitle = jobTitle;
    this.payRate = payRate;
  }

  // The getFullName() method returns a string that combines
  // the first and last names
  getFullName() {
    return this.firstName + " " + this.lastName;
  }
  // The getIntro() method returns a string
  getIntro() {
    let intro =
      "Hi! I'm " + this.getFullName() + " and I am a " +
      this.jobTitle;
    return intro;
  }
  promote (jobTitle, payRate) { 
    this.jobTitle = jobTitle;
    this.payRate = payRate;
  }
}

let employee1 = new Employee(
  1, "Ian", "Auston", "Graphic Artist", 42.50);
  let employee2 = new Employee(
    2, "Cody", "Allison", "Dev", 42.50);

    employee1.promote("Sr. Graphic Artist", 46.75);
console.log(`Job title is ${employee1.jobTitle}`);
console.log(`Pay rate is $${employee1.payRate}`);


console.log(`Employee ${employee1.getFullName()} created`);
console.log(`Job title is ${employee1.jobTitle}`);
console.log(`Pay rate is $${employee1.payRate}`);

let intro = employee1.getIntro();
console.log(intro);

// console.log(`Employee ${employee2.getFullName()} created`);
// console.log(`Job title is ${employee2.jobTitle}`);
// console.log(`Pay rate is $${employee2.payRate}`);