"use strict";
window.onload = init;

function init() {

    const onAddBtn  = document.getElementById("btnadd");
    const onSubBtn  = document.getElementById("btnsub");
    const onMultBtn = document.getElementById("btnmult");
    const onDivBtn = document.getElementById("btndiv");
    
    onAddBtn.onclick = addBtnClicked;
    onSubBtn.onclick = subBtnClicked;
    onMultBtn.onclick = multBtnClicked;
    onDivBtn.onclick = divBtnClicked;

}

function addBtnClicked() {
    const num1            = document.getElementById("idnum1");
    const num2            = document.getElementById("idnum2");
    const idans           = document.getElementById("idans");
    let num1a = Number(num1.value);
    let num2a = Number(num2.value);
    let answerresult = num1a + num2a;
    
     idans.value = answerresult;
}

function subBtnClicked() {
    const num1            = document.getElementById("idnum1");
    const num2            = document.getElementById("idnum2");
    const idans           = document.getElementById("idans");
    let num1a = Number(num1.value);
    let num2a = Number(num2.value);
    let answerresult = num1a - num2a;

    idans.value = answerresult;

}

function multBtnClicked() {
    const num1            = document.getElementById("idnum1");
    const num2            = document.getElementById("idnum2");
    const idans           = document.getElementById("idans");
    let num1a = Number(num1.value);
    let num2a = Number(num2.value);
    let answerresult = num1a * num2a;

    idans.value = answerresult;

}
function divBtnClicked() {
    const num1            = document.getElementById("idnum1");
    const num2            = document.getElementById("idnum2");
    const idans           = document.getElementById("idans");
    let num1a = Number(num1.value);
    let num2a = Number(num2.value);
    let answerresult = num1a / num2a;

    idans.value = answerresult;


}


