"use strict";
window.onload = init;

function init() {

    const onCone = document.getElementById("cone");
    const onCup = document.getElementById("cup");
    onCone.onclick = myFunction;
    onCup.onclick = myFunction;

    // i think i should move this elsewhere but idk where
    const getprice = document.getElementById("submitbutton");
    submitbutton.onclick = getToppingsCost;
}

function myFunction() {
    // Get the checkbox
    var checkBox = document.getElementById("cone").checked;
    // added this in attempt to force a cone cup choice before price is shown
    var checkBox2 = document.getElementById("cup").checked;
    // If the checkbox is checked, display the output text
    if (checkBox == true) {
        document.getElementById("toppings").style.display = "none";
        // clear topping fields - update this appears to be working
        document.getElementById("sprinkles").checked = false;
        document.getElementById("whippedCream").checked = false;
        document.getElementById("hotFudge").checked = false;
        document.getElementById("cherry").checked = false;
    }
    else {
        document.getElementById("toppings").style.display = "block";
    }
}

// all of this is working - toppins do clear between toggle from cup/cone 
// need to add something to ensure scoops are set to at least 1 before calculating price
function getToppingsCost() {
    const sprinklescost = document.getElementById("sprinkles").checked;
    const whippedCreamcost = document.getElementById("whippedCream").checked;
    const hotFudgecost = document.getElementById("hotFudge").checked;
    const cherrycost = document.getElementById("cherry").checked;
    const numofScoops = document.getElementById("numScoops");
    let toppingAmt = 0;

    // first scoop price
    const firstScoopPrice = 2.25;
    console.log(numofScoops.value);
    // additional scoops price 
    let numofextraScoops = Number(numofScoops.value - 1);
    let numofextraScoopsPrice = (numofextraScoops * 1.25);
    // this works thus far

    if (sprinklescost) {
        toppingAmt += (.50);
    }
    if (whippedCreamcost) {
        toppingAmt += (.25);
    }
    if (hotFudgecost) {
        toppingAmt += (1.25);
    }
    if (cherrycost) {
        toppingAmt += (.25);
    }
    if (numofScoops.value > 0)  {
    baseprice.innerHTML = `Base Cost ${firstScoopPrice + numofextraScoopsPrice + toppingAmt}`;
    taxprice.innerHTML = `Tax Cost ${(firstScoopPrice + numofextraScoopsPrice + toppingAmt) * .06}`;
    totaldue.innerHTML = `Total Due ${(firstScoopPrice + numofextraScoopsPrice + toppingAmt) * 1.06}`;
    }
    else { 
        alert("Please enter the amount of scoops, up to 4!");

    }

}