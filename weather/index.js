let cities = [
    {
        name: "Benbrook, TX",
        latitude: 32.6732,
        longitude: -97.4606
    },
    {
        name: "Detroit, MI",
        latitude: 42.331429,
        longitude: -83.04866
    },
    {
        name: "Los Angeles, CA",
         latitude: 34.0430175,
         longitude: -118.2694428
    },
    {
        name: "New York, NY",
        latitude: 40.77311245309296, 
        longitude: -73.97696979271693
    },
    // {
    //     name: "Paris, France",
    //     latitude: 48.856613,
    //     longitude: 2.352222
    // },
    // {
    //     name: "Tokyo, Japan",
    //     latitude: 35.689487,
    //     longitude: 139.691711
    // },
    // {
    //     name: "Cairo, Egypt",
    //     latitude: 30.044420,
    //     longitude: 31.235712
    // },
];

window.onload = function () {
    initCityDropdown(cities);

};


function initCityDropdown(cities) {

    const citylist = document.getElementById("citiesweather");
    let cityLength = cities.length;
    for (let i = 0; i < cityLength; i++) {
        let citiesoption = new Option(cities[i].name);
        citylist.appendChild(citiesoption);
    }
}

citiesweather.onchange = loadweatherinfo;

function loadweatherinfo() {
    let tbody = document.getElementById("weatherTable");
    tbody.innerHTML = "";
    
    // Find the table
    // let TblHead = document.getElementById("TableHead");
    let currentcityIndex = document.getElementById("citiesweather").selectedIndex-1;

    let stationLookupUrl = ("https://api.weather.gov/points/" + cities[currentcityIndex].latitude + "," + cities[currentcityIndex].longitude);
    alert(stationLookupUrl);
    // let url = "https://jsonplaceholder.typicode.com/users";
    fetch(stationLookupUrl)
        .then((response) => response.json())
        .then((data) => parseObject(data));
};
// loop through the array


function parseObject(data) {
    fetch(data.properties.forecast)
        .then(response => response.json())
        .then(data => forecast(data));
}

function forecast(data) {
     let tbody = document.getElementById("weatherTable");


    for (let i = 0; i < data.properties.periods.length; i++) {

        let row = tbody.insertRow(-1);

                // Create new cells (<td> elements) and add text
                let cell1 = row.insertCell(0);
                cell1.innerHTML = "Temperature" + " " + data.properties.periods[i].temperature + " " + data.properties.periods[i].temperatureUnit;
                let cell2 = row.insertCell(1);
                cell2.innerHTML = data.properties.periods[i].name;
                // let cell3 = row.insertCell(2);
                // cell3.innerHTML = data[i].email;
                // let cell4 = row.insertCell(3);
                // cell4.innerHTML = data[i].address.street;
                // let cell5 = row.insertCell(4);
                // cell5.innerHTML = data[i].address.city;
                // let cell6 = row.insertCell(5);
                // cell5.innerHTML = data[i].address.zipcode;


        // tbody.innerHTML =  data.properties.periods[i].temperature;
        // tbody.innerHTML =  data.properties.periods[i].name;

        // let row = tbody.insertRow(-1);

        // let cell1 = row.insertCell(0);
        // cell1.innerHTML = cell1.innerHTML + data.properties.gridY[i];
        //  buildrows(tbody, data[i]);
    }

}

