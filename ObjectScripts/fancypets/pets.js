var tasks = [{
   name: "Rubby",
   type: "Dog",
   breed: "Corgi",
   bestTrick: "Tug of war",
   image: "pet-images/rubby.jpg"
},
{
   name: "Howdy",
   type: "Dog",
   breed: "Mixed Breed",
   bestTrick: "Go find it!",
   image: "pet-images/howdy.jpg"
},
{
   name: "KitKit",
   type: "Cat",
   breed: "American Shorthair",
   bestTrick: "Commanding his owner to feed him",
   image: "pet-images/kitkit.jpg"
},
{
   name: "Lil' Miss",
   type: "Cat",
   breed: "Tabby",
   bestTrick: "Looking aloof",
   image: "pet-images/lilmiss.jpg"
},
{
   name: "Happy",
   type: "Dog",
   breed: "Golden Retriever",
   bestTrick: "Refusing to retrieve!",
   image: "pet-images/happy.jpg"
},
{
   name: "Piper",
   type: "Dog",
   breed: "Beagle",
   bestTrick: "Find it!  Dropped food edition!",
   image: "pet-images/piper.jpg"
},
{
   name: "Spooky",
   type: "Cat",
   breed: "Mixed",
   bestTrick: "Gymnastics!",
   image: "pet-images/spooky.jpg"
}
];

let cardContainer;

let createTaskCard = (task) => {

   let card = document.createElement('div');
   card.className = 'card shadow cursor-pointer';

   let cardBody = document.createElement('div');
   cardBody.className = 'card-body';

   let name = document.createElement('h5');
   name.innerText = task.name;
   name.className = 'card-title';

   let type = document.createElement('div');
   type.innerText = "Type:" + " " + task.type;
   type.className = 'card-type';

   let breed = document.createElement('div');
   breed.innerText = "Breed:" + " " + task.breed;
   breed.className = 'card-breed';

   let bestTrick = document.createElement('div');
   bestTrick.innerText = "Best Trick:" + " " + task.bestTrick;
   bestTrick.className = 'card-bestTrick';

   let image = document.createElement('img');
   image.width = 300;
   image.src = task.image;
   image.className = 'card-image';



   cardBody.appendChild(name);
   cardBody.appendChild(type);
   cardBody.appendChild(breed);
   cardBody.appendChild(bestTrick);
   cardBody.appendChild(image);



   card.appendChild(cardBody);
   cardContainer.appendChild(card);

}

let initListOfTasks = () => {
   if (cardContainer) {
      document.getElementById('card-container').replaceWith(cardContainer);
      return;
   }

   cardContainer = document.getElementById('card-container');
   tasks.forEach((task) => {
      createTaskCard(task);
   });
};

initListOfTasks();
