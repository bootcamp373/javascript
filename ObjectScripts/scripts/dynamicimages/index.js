"use strict"

let imageFiles = [
    { name: "images/black.jpg", description: "A black cat" },
    { name: "images/grey.jpg", description: "A grey cat" },
    { name: "images/orange.jpg", description: "A orange cat" },
    { name: "images/white.jpg", description: "A white cat" },

];

window.onload = function () {

    let nodescription = document.querySelectorAll('img');
    for (let i = 0; i < nodescription.length; i++) {
        nodescription[i].description = "cat";
    }
    initDropdown(imageFiles);
    displayBtn.onclick = addCat;
    displayBtn2.onclick = removeCat;

};

function initDropdown(imageFiles) {

    const catlist = document.getElementById("catlist");
    let catsLength = imageFiles.length;
    for (let i = 0; i < catsLength; i++) {
        let catOption = new Option(imageFiles[i].name, imageFiles[i].description);
        catlist.appendChild(catOption);
    }
}

function addCat() {
    let imgToAdd = document.getElementById("catlist");
    let selected = imgToAdd.selectedIndex;
    let picDiv = document.getElementById("imagesdiv");
    let img = document.createElement("img");
    img.src = imgToAdd.options[selected].text;
    img.alt = imgToAdd.options[selected].value;
    picDiv.appendChild(img);
    return false;
}

function removeCat() {
    document.getElementById("imagesdiv").innerHTML = "";
}









































// function initTeamsDropdown(teams) {

//     const footballList = document.getElementById("footballList");
//     let teamsLength = teams.length;
//     for (let i = 0; i < teamsLength; i++) {
//         let teamOption = new Option(teams[i].name, teams[i].code);
//         footballList.appendChild(teamOption);
//     }
// }

// function addTeams() {
//     const footballList = document.getElementById("footballList");
//     const teamname = document.getElementById("teamname").value;
//     const teamcode = document.getElementById("teamcode").value;
//     const teamcity = document.getElementById("teamcity").value;

//     let teamOption = new Option(teamname, teamcode, teamcity);
//     footballList.appendChild(teamOption);

//     teams.push({
//         code: teamcode,
//         name: teamname,
//         plays: teamcity
//     });
//     return false;
// }


// function displayInfo() {
//     const footballList = document.getElementById("footballList");
//     const selectedOptions = footballList.selectedOptions;
//     let matchingOptionCodes = [];
//     let numTeams = teams.length;
//     debugger;
//     //for each selected option, look up info from teams array and display football info
//     for (let i = 0; i < selectedOptions.length; i++) {
//         matchingOptionCodes.push(selectedOptions[i].value);
//     }

//     for (let i = 0; i < numTeams; i++) {
//         debugger;
//         if (matchingOptionCodes.includes(teams[i].code)) {
//             console.log(teams[i]);
//             let message = ` You selected the : ${teams[i].name}  ${teams[i].code} Who play in: ${teams[i].plays}`;
//             footballInfo.innerHTML = message;
//         }
//     }
//     return false;
// }