"use strict"

let teams = [
{code:"DAL", name:"Dallas Cowboys", plays:"Arlington, TX"},
{code:"DEN", name:"Denver Broncos", plays:"Denver, CO"},
{code:"HOU", name:"Houston Texans", plays:"Houston, TX"},
{code:"KAN", name:"Kansas City Chiefs", plays:"Kansas City, MO"}
];

window.onload = function () {
    const displayBtn = document.getElementById("displayBtn");
    initTeamsDropdown(teams);
    displayBtn.onclick = displayInfo;
    addBtn.onclick = addTeams;
};

function initTeamsDropdown(teams) {

    const footballList = document.getElementById("footballList");
    let teamsLength = teams.length;
    for (let i = 0; i < teamsLength; i++) {
        let teamOption = new Option(teams[i].name, teams[i].code);
        footballList.appendChild(teamOption);
    }
}

function addTeams() {
    const footballList = document.getElementById("footballList");
    const teamname = document.getElementById("teamname").value;
    const teamcode = document.getElementById("teamcode").value;
    const teamcity = document.getElementById("teamcity").value;
    
    let teamOption = new Option(teamname, teamcode, teamcity);
    footballList.appendChild(teamOption);

    teams.push({
        code: teamcode,
        name: teamname,
        plays: teamcity
    });
    return false;
}


function displayInfo(){
    const footballList = document.getElementById("footballList");
    const selectedOptions = footballList.selectedOptions;
    let matchingOptionCodes = [];
    let numTeams = teams.length;
    debugger;
    //for each selected option, look up info from teams array and display football info
    for (let i = 0; i < selectedOptions.length; i++) {
        matchingOptionCodes.push(selectedOptions[i].value);        
    }
    
    for(let i = 0; i < numTeams; i++) {
        debugger;
        if (matchingOptionCodes.includes(teams[i].code)) {
            console.log(teams[i]);
            let message = ` You selected the : ${teams[i].name}  ${teams[i].code} Who play in: ${teams[i].plays}`;
            footballInfo.innerHTML = message;
        }
    }
    return false;
}