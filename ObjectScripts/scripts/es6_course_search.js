let courses = [
    {
        CourseId: "PROG100",
        Title: "Introduction to HTML/CSS/Git",
        Location: "Classroom 7",
        StartDate: "09/08/22",
        Fee: "100.00",
    },
    {
        CourseId: "PROG200",
        Title: "Introduction to JavaScript",
        Location: "Classroom 9",
        StartDate: "11/22/22",
        Fee: "350.00",
    },
    {
        CourseId: "PROG300",
        Title: "Introduction to Java",
        Location: "Classroom 1",
        StartDate: "01/09/23",
        Fee: "50.00",
    },
    {
        CourseId: "PROG400",
        Title: "Introduction to SQL and Databases",
        Location: "Classroom 7",
        StartDate: "03/16/23",
        Fee: "50.00",
    },
    {
        CourseId: "PROJ500",
        Title: "Introduction to Angular",
        Location: "Classroom 1",
        StartDate: "04/25/23",
        Fee: "50.00",
    }
];

let PROG200Date = (courses.find(isPROG200)).StartDate;
console.log("PROG200s start date is :" + PROG200Date);

function isPROG200(arrayValue) {
    if (arrayValue.CourseId == "PROG200") {
        return true;
    }
    else {
        return false;
    }
}

let PROJ500Title = (courses.find(isPROJ500)).Title;
console.log("PROJ500s Title is :" + PROJ500Title);

function isPROJ500(arrayValue) {
    if (arrayValue.CourseId == "PROJ500") {
        return true;
    }
    else {
        return false;
    }
}

// let cheapcourses = courses.filter(findcheapcourses);
// let numItems = courses.length;


// for (let i = 0; i < numItems; i++) {
//     console.log("cheap course Titles :" + cheapcourses[i].Title);
// }
// function findcheapcourses(arrayValue) {


//         if (arrayValue.Fee <= 50.00) {
//             return true;
//         }
//         else {
//             return false;
//         }
//     }

let classroom1 = courses.filter(findclassroom1);
let numItems = courses.length;

for (let i = 0; i < numItems; i++) {
    console.log("Classroom 1 courses :" + (classroom1[i].Title));
    
}
function findclassroom1(arrayValue) {

        if (arrayValue.Location == "Classroom 1") {
            return true;
        }
        else {
            return false;
        }
    }

