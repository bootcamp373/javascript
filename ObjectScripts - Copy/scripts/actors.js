let academyMembers = [
    {
        memID: 101,
        name: "Bob Brown",
        films: ["Bob I", "Bob II", "Bob III", "Bob IV"]
    },
    {
        memID: 142,
        name: "Sallie Smith",
        films: ["A Good Day", "A Better Day"]
    },
    {
        memID: 187,
        name: "Fred Flanders",
        films: ["Who is Fred?", "Where is Fred?",
            "What is Fred?", "Why Fred?"]
    },
    {
        memID: 203,
        name: "Bobbie Boots",
        films: ["Walking Boots", "Hiking Boots",
            "Cowboy Boots"]
    },
];

let numItems = academyMembers.length;
for (let i = 0; i < numItems; i++) {
    if (academyMembers[i].memID == 187) {
        console.log(academyMembers[i].name);
    }
}

for (let i = 0; i < numItems; i++) {
    if (academyMembers[i].films.length >= 3) {
        console.log(academyMembers[i].memID);
    }
}

for (let i = 0; i < numItems; i++) {
    if (academyMembers[i].name.startsWith("Bob")) {
        console.log("name includes Bob" + " " + academyMembers[i].name);
    }
}


